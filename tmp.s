    .file "tmp.s"
    .global main 
    .align 2

main:
    str x30, [sp,-16]!
    mov x19, xzr //counter
top:
    cmp x19, 10 //compare x19 (counter) with 10
    bge end //bgt greater than, bge or equal to, btl less than, bte or equal to
    ldr x0, =s //load register x0 with symbol s from memory
    mov x1, x19 //the printf argument
    bl  printf
    add x19, x19, 1 //add to x19 the values of x19 and 1
    b   top
end:
    ldr x30, [sp], 16
    mov x0, xzr
    ret

    .data
s:  .asciz "%d\n"

    .end
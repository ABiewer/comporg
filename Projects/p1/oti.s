.arch armv8-a
.file "oti.s"
.global main
.align 2
//
main:
    str x30, [sp,-16]!
    mov x20, x1 //argv[] pointer
Loop:
    ldr x0, [x20, 8]! //x0 needs the next argument, and x20 needs to point to the next char* (8bytes)
    cbz x0, _end    //if x0 is 0, there are no more arguments, end loop
    bl  puts
    bl  Loop
_end:
    ldr x30, [sp], 16
    ret
.end

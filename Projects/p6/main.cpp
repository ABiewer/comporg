#include "fs.h"
#include "stat.h"
#include "types.h"

#include <iostream>
#include <set>
#include <fstream>

using namespace std;

bool isBlockFree(uchar b, int nb);
uchar getByteFromBitmap(uint dataNum, void* dataBlock); 
uchar* getDataBlock(uint dataBlockNum, void* allDataBlocks);

bool recursiveInodeWrapper(dinode* root, void* allDataBlocks, superblock* superptr);
bool recursiveChecker(dinode* inode, dinode* parent, dinode* root, set<ushort>& inums, void* allDataBlocks);

bool checkAllDataBlocks(dinode* root, void* allDataBlocks, void* bitmapBlock, superblock* superptr);
bool isFreeDataBlock(void* bitmapBlock, uint nb);
bool VerboseIsFreeDataBlock(void* bitmapBlock, uint nb);

int main(int argc, char *argv[])
{
    int retval = 0;
    set<uint> InodeAllocatedDataBlocks;

    /*File Validation----------------------------------------------------------------------------------------------*/
    //make sure there is a valid file given
    if (argc < 2)
    {
        cout << "Invalid number of arguments" << endl;
        return 1;
    }

    //open up the file given in argv[1]
    ifstream fileSystem;
    fileSystem.open(argv[1]);

    //make sure the file was opened
    if (!fileSystem.is_open())
    {
        cout << "Invalid file name" << endl;
        return 1;
    }


    /*Super Block Validation---------------------------------------------------------------------------------------*/
    //read the super block from the file
    char superBuff[BSIZE * 2];
    void *ptr = superBuff + BSIZE;

    fileSystem.read(superBuff, BSIZE * 2);
    superblock *superptr = (superblock *)ptr;

    //print out the meta data
    cout << "File name: " << argv[1] << endl;
    cout << "Size: " << superptr->size << endl;
    cout << "Number of data blocks: " << superptr->nblocks << endl;
    cout << "Number of inodes: " << superptr->ninodes << "\n--------------------\n" << endl;

    //make sure the file sizes match
    fileSystem.seekg(0, fileSystem.end);
    uint fileSize = (uint)fileSystem.tellg();

    if ((superptr->size) * BSIZE != fileSize)
    {
        cout << "The size given in the super block does not match the size of the file. " << endl;
        retval = 1;
    }
    //fileSystem.seekg(0,fileSystem.beg);

    /*Inodes -----------------------------------------------------------------------------------------------------*/
    //get all the inodes into memory
    void *allInodes = malloc(superptr->ninodes * sizeof(dinode));
    fileSystem.seekg(BSIZE * 2, fileSystem.beg);
    fileSystem.read((char *)allInodes, sizeof(dinode) * superptr->ninodes);
    dinode *inode = (dinode *)allInodes;
    //inode 0 is empty
    inode++;

    //inode 1 better be a directory
    if(inode->type != T_DIR){
        cout<<"The root inode is not a directory/is not allocated. "<< endl;
        retval = 1;
    }

    //read in the data block bitmap
    void* bitmapBlock = malloc(BSIZE);
    //there are 2 blocks before the Inodes and 1 after, before the bitmap Block
    fileSystem.seekg(BSIZE * (2 + 1) + superptr->ninodes * sizeof(dinode), fileSystem.beg);
    fileSystem.read((char*)bitmapBlock, BSIZE);

    //read in the data blocks
    void* dataBlocks = malloc(BSIZE*superptr->nblocks);
    fileSystem.seekg(BSIZE* (2+1+1) + superptr->ninodes * sizeof(dinode), fileSystem.beg);
    fileSystem.read((char*)dataBlocks, BSIZE*superptr->nblocks);

    //inode 1's first data block should have 2 entries, . and ..
    dirent* rootDirEntry = (dirent*) (getDataBlock(inode->addrs[0],dataBlocks));
    string rootName = ".";
    string rootDirEntryName = (*rootDirEntry).name;
    if( (*rootDirEntry).inum != 1 || rootName != rootDirEntryName ){
        cout<<"The root directory does not refer to itself as '.'"<<endl;
        retval = 1;
    }
    string rootParentName = "..";
    rootDirEntry++;
    rootDirEntryName = (*rootDirEntry).name;
    if( (*rootDirEntry).inum != 1 || rootDirEntryName != rootParentName ){
        cout<<"The root directory does not refer to itself as '..'"<<endl;
        retval = 1;
    }

    /*
    //print out inode info
    string inodeType = "null";
    switch (inode->type)
    {
    case T_DIR:
        inodeType = "directory";
        break;
    case T_FILE:
        inodeType = "file";
        break;
    case T_DEV:
        inodeType = "special device";
        break;
    }

    cout << "Inode type: " << inodeType.c_str() << endl;
    cout << "Inode link: " << inode->nlink << endl;
    cout << "Inode size: " << inode->size << endl;*/

    /*if(inode->type==T_DIR){
        uint dataNum = inode->addrs[0];
        cout<<"\nFirst data block number: " << dataNum<<endl;
        uchar bitmapByte = getByteFromBitmap(dataNum, bitmapBlock);
        cout<<"Contained in byte: " << hex << (uint)bitmapByte << dec << endl;
        cout<<"1 for free, 0 for used: " << isBlockFree(bitmapByte, dataNum%8) << endl;
    }*/
    inode = ((dinode*) allInodes)+1;
    for(uint i = 0;i<superptr->ninodes;i++){
        if(inode->type == T_DIR || inode->type == T_FILE){

            //inode Number
            ushort inodeNum = (ushort) (inode - (dinode*)allInodes);

            //check the first entry in the inode is . and .. and . refers to that inode
            if(inode->type == T_DIR){
                dirent* entry = (dirent*)getDataBlock(inode->addrs[0], dataBlocks);
                if(entry->inum != inodeNum)
                {
                    cout<<"The Inode numbered " << inodeNum << " does not have '.' refer to itself." << endl;
                    retval = 1;
                }
                string fileName = ".";
                string inodeReferenceName = entry->name;
                if(fileName!=inodeReferenceName){
                    cout<<"The Inode numbered " << inodeNum << " does not refer to itself as '.'"<<endl;
                    retval = 1;
                }
                entry++;
                fileName = "..";
                inodeReferenceName = entry->name;
                if(fileName!=inodeReferenceName){
                    cout<<"The Inode numbered " << inodeNum << " does not have a reference '..'"<<endl;
                    retval = 1;
                }

                //verify all file names are ASCII (>=32, <=126, || == 0)
                for(uint i = 0;i<BSIZE/sizeof(dirent)-2;i++){
                    entry++;
                    //if the file is not a file, then don't check it
                    if(entry->inum==0){
                        break;
                    }
                    
                    //the first letter better not be 0, and 
                    if(entry->name[0] == '\0'){
                        cout<<"In the directory with inode number "<< inodeNum << ", the file with inode number "<<entry->inum<<" has an invalid name " << entry->name<<endl;
                    }
                    for(int j = 0;j<DIRSIZ;j++){
                        char current = entry->name[j];
                        if(!(current==0||current>=32||current<=127)){
                            cout<<"In the directory with inode number " << inodeNum << ", the file with inode number "<<entry->inum<<" has an invalid name "<<entry->name<<endl;
                        }
                    }
                }

            }

            //checking for valid files
            if(inode->type == T_FILE){
                if(inode->size>MAXFILE*BSIZE){
                    cout<<"The inode numbered "<< inodeNum << " has a size: "<<inode->size<<" which is larger than the \
maximum allowed file size: "<< MAXFILE*BSIZE<<endl;
                }
            }

            //check for duplicate references from inodes to data blocks
            for(int i = 0;i<12;i++){
                uint dataNum = inode->addrs[i];
                if(dataNum == 0){
                    //the data block is not used, so the following data blocks will not be used either. 
                    break;
                }
                if(InodeAllocatedDataBlocks.find(dataNum)!=InodeAllocatedDataBlocks.end()){
                    cout<<"There were multiple reference to data block " << dataNum << " in the inodes." << endl;
                    retval = 1;
                }
                InodeAllocatedDataBlocks.insert(dataNum);
            }
            inode ++;
        }
    }


    //now checking to make sure the tree of directories is correct/valid
    inode = ((dinode*) allInodes)+1;
    if(!recursiveInodeWrapper(inode, dataBlocks, superptr)){
        retval = 1;
    }

    //cout<<"Doing Checking all data"<<endl;
    if(!checkAllDataBlocks(inode, dataBlocks, bitmapBlock, superptr)){
        retval = 1;
    }
    //cout<<"Ending checking all data"<<endl;

    //memory management
    std::free(allInodes);
    std::free(bitmapBlock);
    std::free(dataBlocks);
    fileSystem.close();

    if(retval == 0){
        cout<<"There were no errors detected in the file given."<<endl;
    }

    return retval;
}

//used to get the byte containing the bit from the bitmap
uchar getByteFromBitmap(uint dataNum, void* bitMapBlock){
    dataNum = dataNum-29; //there are 29 blocks before the data blocks, and the addresses reference them this way
    uchar* byte = (uchar*) bitMapBlock;
    byte+=(dataNum/8);
    return *byte;
}

bool isBlockFree(uchar b, int nb){
    return (bool)((b & (uchar)(0x1<<nb))>>nb);
}

uchar* getDataBlock(uint dataBlockNum, void* allDataBlocks){
    uchar * retval = (uchar*) allDataBlocks;
    retval += (dataBlockNum-29)*BSIZE;
    return retval;
}

bool recursiveInodeWrapper(dinode* root, void* allDataBlocks, superblock* superptr){
    //set of inode numbers to keep track of what has already been visited.
    set<ushort> inums;
    ushort inum = (ushort)(root-root+1);
    inums.insert(inum);

    //for each direct address in the root inode
    for(int i = 0;i<12;i++){
        //get the directoryBlock from the data blocks
        uint directoryBlock = root->addrs[i];
        if(directoryBlock == 0){
            //reached the last possible directory, so return (in the root case, there are no files in the file system)
            return true;
        }
        dirent* nextDir = (dirent*) getDataBlock(directoryBlock, allDataBlocks);

        //accounting for '.' and '..' as the first two entries of the directory
        nextDir+=2;

        //go through each directory entry and make sure it is null or run the recursive rules on it
        for(uint j = 0;j<BSIZE/sizeof(dirent)-2;j++){
            if(nextDir->inum == 0){
                break;
            }
            dinode* nextInode = root + nextDir->inum - 1;
            if(!recursiveChecker(nextInode, root, root, inums, allDataBlocks)){
                return false;
            }
            nextDir++;
        }

    }

    //now we have a list of all the referenced inodes, check to make sure all the inodes are valid
    dinode* inode = root;
    set<ushort> inumsInInodes;
    for(uint i = 0;i<superptr->ninodes-1;i++){
        if(inode->type!=0){
            uint inum = (uint) (inode-root+1);
            if(inums.find(inum)==inums.end()){
                cout<<"The inode: "<<inum<<" was marked valid but was not found in any of the directories."<<endl;
                return false;
            }
            inumsInInodes.insert(inum);
        }
    }

    //make sure the opposite is true, all the referenced inodes in directories are actually valid inodes in the array
    for(auto iter = inums.begin();iter!=inums.end();iter++){
        if(inumsInInodes.find(*iter)==inumsInInodes.end()){
            cout<<"The inode: "<<*iter<<" is referenced by a directory but is not a valid inode."<<endl;
            return false;
        }
    }

    //there are fewer than (12*512/16 =) 384 inode numbers, so the indirect address is not used in these files. 

    return true;
}

bool recursiveChecker(dinode* inode, dinode* parent, dinode* root, set<ushort>& inums, void* allDataBlocks){
    if(inode->type!=T_DIR){
        return true;
    }
    ushort inum = (ushort) (inode-root+1);
    if(inums.find(inum)!=inums.end()){
        cout<<"The directory with inode number "<<inum<<" was referenced improperly (found in more than 1 directory)."<< endl;
        return false;
    }
    inums.insert(inum);

    //for each direct address in the inode
    for(int i = 0;i<12;i++){
        uint directoryBlock = inode->addrs[i];
        if(directoryBlock == 0){
            return true;
        }
        dirent* nextDir = (dirent*) getDataBlock(directoryBlock, allDataBlocks);

        //accounting for '.' and '..'
        nextDir+=2;

        //all following inodes that are not 0 and are directories better not be listed twice
        for(uint j = 0;j<BSIZE/sizeof(dirent) - 2;j++){
            if(nextDir->inum == 0){
                break;
            }
            dinode* nextInode = root + nextDir->inum - 1;
            if(!recursiveChecker(nextInode, inode, root, inums, allDataBlocks)){
                return false;
            }
            nextDir++;
        }
    }

    //don't need to check indirect directories (inode # too small)

    return true;
}


bool checkAllDataBlocks(dinode* root, void* allDataBlocks, void* bitmapBlock, superblock* superptr){
    dinode* inode = root;
    set<uint> referencedDataBlocks;
    uint numInodes = superptr->ninodes-1;
    for(uint j = 0;j<numInodes;j++){
        //cout<<"Good"<<endl;
        for(uint i = 0;i<12;i++){
            uint address = inode->addrs[i];
            if(address == 0){
                break;
            }
            if(address>superptr->size){
                cout<<"The block number: "<<address<<" in inode: "<<j+1<<" is out of range of the file system."<<endl;
                return false;
            }

            //check data block for duplicates
            if(referencedDataBlocks.find(address)!=referencedDataBlocks.end()){
                cout<<"The data block numbered: "<<address<<" is referenced multiple times in inodes."<<endl;
                return false;
            }
            referencedDataBlocks.insert(address);

            //check bitmap for valid data block
            if(isFreeDataBlock(bitmapBlock, address-29)){
                cout<<"The data block numbered: "<<address<<" is contained in an inode, but is not marked in the bitmap."<<endl;
                return false;
            }
        }
        //indirects
        uint indirect = inode->addrs[12];
        if(indirect == 0){
            inode++;
            continue;
        }
        if(indirect>superptr->size){
            cout<<"The indirect reference: "<<indirect<<" in inode: "<<(uint)(inode-root)<<" is larger than the file size: "<<superptr->size<<endl;
            return 1;
        }
        referencedDataBlocks.insert(indirect);
        uint* addressPtr = (uint*)allDataBlocks + BSIZE/sizeof(uint)*(indirect-29);
        for(uint i = 0;i<BSIZE/sizeof(uint);i++){
            //cout<<indirect<<endl;
            //cout<<"difference: " << addressPtr-(uint*)allDataBlocks<<endl;
            //cout<<allDataBlocks<<endl;
            //cout<<((uint*)allDataBlocks+indirect)<<endl;
            uint address = *addressPtr;
            if(address == 0){
                break;
            }
            if(address>superptr->size){
                cout<<"The block number: "<<address<<" in inode: "<<j+1<<" is out of range of the file system."<<endl;
                return false;
            }
            //check data block for duplicates
            if(referencedDataBlocks.find(address)!=referencedDataBlocks.end()){
                //cout<<"INDIRECT"<<endl;
                cout<<"The data block numbered: "<<address<<" is referenced multiple times in inodes."<<endl;
                return false;
            }
            referencedDataBlocks.insert(address);

            //check bitmap for valid data block
            if(isFreeDataBlock(bitmapBlock, address-29)){
                cout<<"INDIRECT"<<endl;
                cout<<"The data block numbered: "<<address<<" is contained in an inode, but is not marked in the bitmap."<<endl;
                return false;
            }
            addressPtr++;
        }
        inode++;
    }

    //all blocks referenced by inodes are now in that array, so we can check the bitmap against that
    for(uint i = 29;i<370;i++){
        bool isFree = isFreeDataBlock(bitmapBlock, i-29);
        
        if(!isFree){
            if(referencedDataBlocks.find(i)==referencedDataBlocks.end()){
                cout<<"The data block: " << i<< " was marked as taken by the bitmap, but is not claimed by any inode."<<endl;
                return false;
            }
        }
    }
    return true;
}

bool isFreeDataBlock(void* bitmapBlock, uint nb){
    uint* bytePtr = (uint*) bitmapBlock;
    bytePtr+=(nb/(uint)32);
    return !((bool) ((*bytePtr & ( 0x1 << nb%32 )) >> nb%32));
}


bool VerboseIsFreeDataBlock(void* bitmapBlock, uint nb){
    uint* bytePtr = (uint*) bitmapBlock;
    cout<<"nb "<<nb<<endl;
    cout<<"Addresses:"<<bytePtr<< " " << bitmapBlock<<endl;
    bytePtr+=(nb/(uint)32);
    cout<<"After adding Addresses:"<<bytePtr<< " " << bitmapBlock<<endl;
    cout<<"Values: " << hex<<*bytePtr<<endl;
    cout<<nb%32<<" " << (0x1<<nb%32) << endl;
    cout<<(*bytePtr&(0x1<<nb%32))<<dec<<endl;
    return !((bool) ((*bytePtr & ( 0x1 << nb%32 )) >> nb%32));
}

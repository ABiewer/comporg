    .text
    .global main
    //.global buffer // - is this needed?
    .align 2

    .EQU    TAILLEN, 10


main:
    /*
    List of registers and what they contain for this program
    x19 - indicator whether to continue with read loop (1) or to end the file (0)
    x20 - file descriptor
    x21 - head of tailbuffer
    x22 - current position in tailbuffer
    x23 - pointer for storagebuffer
    x24 - temporary pointer to the latest line of text for insertion into tailbuffer
    x25 - value representing whether or not the tail buffer has been cycled through
    x26 - pointer to the contents of the storage buffer
    x27 - pointer to the contents of the latest line buffer
    */



    //store x30, x20, x19, and x21, x22 so I don't lose them
    stp x30, x20, [sp, -32]!
    stp x19, x23, [sp, -32]!
    stp x21, x22, [sp, -32]!
    stp x24, x20, [sp, -32]!
    stp x25, x26, [sp, -32]!


    //load argv[1] into x0, 0 into x1 for open()
    ldr x0, [x1, 8]
    mov x1, xzr

    //check argv[1] for file name (usage for when no arguments given)
    cbz x0, err_usage

    //open file
    bl  open
    /*
    int open(char* pathname, flags)

    Opens the file given by pathname, with the read-only flag
    Parameters:
        x0 - pathname from argv[1]
        x1 - flags, set to 0 ie O_RDONLY

    Returns:
        x0 - -1 when an error occurs
    */

    //check opened file (badopen for bad file, also using perror())
    mov w11, -1
    cmp w0, w11
    beq err_badopen

    //x0 is the file descriptor, move to x20 to preserve it
    mov x20, x0 
    
    //move TAILLEN to x0 to allocate TAILLEN sized memory
    ldr x0, =TAILLEN
    mov x8, 8
    mul x0, x0, x8

    bl  malloc
    /*
    void * malloc(size_t size);

    allocates memory for the tail buffer for the exact size of the TAILLEN

    Parameters:
        x0 - the size of the allocated array, TAILLEN* (1 byte per char*)

    Returns:
        x0 - pointer to location allocated
    */

    //if malloc returns 0, there was an issue allocating space
    cbz x0, err_badtail

    //if malloc actually returned an array, save it
    //it is saved twice, once to preserve the start of the array, and once to preserve the current location
    //while looping through reading and writing
    mov x21, x0
    mov x22, x0

    //x23 is the pointer to the current location in the storage buffer, starting at the beginning
    ldr x23, =storagebuffer

    //there needs to be an indicator whether or not the tailbuffer has been overwritten
    //at any point, so that we know where to start reading from at the end of this file
    mov x25, xzr

read_loop:
    //we will be checking whether there is a newline or end of file, so we set x19 to 0,
    //assuming that we reached the end of the file. 
    mov x19, xzr

    //x20 holds the file descriptor
    mov x0, x20
    //readbuffer is 1 byte, and that's where read is going to put stuff
    ldr x1, =readbuffer
    //count needs to be 1, for 1 byte
    mov x2, 1

    //read one byte at a time
    bl  read
    /*
    ssize_t read(int fd, void* buf, size_t count);

    reads the next character in the file

    Parameters:
        x0 - file descriptor, stored in x20
        x1 - buffer pointer, the place called buffer
        x2 - the amount of bytes to be read (1)

    Returns:
        x0 - the number of bytes that it returned, if it's 0, it hit the end of the file
    */

    //these are the two conditions that escape the read loop
    //if read returns 0, the end of the file has been reached. 
    cbz x0, end_of_file    //TODO: fix result!=0
    //if the read buffer returns with a newline, we need to do memory management
    ldr x11, =newlinechar
    ldrb w11, [x11]
    ldr x18, =readbuffer
    ldrb w12, [x18]
    cmp w11, w12
    beq newline

    //if the storage buffer is filled, then do not add or change anything in it
    ldr x18, =storagebuffer
    sub x11, x23, x18
    cmp x11, 4095
    //the storage doesn't need to be overwritten, so just skip to reading again
    beq read_loop

   //if we are going to write to the storage buffer, we do that here
    mov x0, x23
    ldr x18, =readbuffer
    ldr x1, [x18]
    mov x2, 1

    bl  memset
    /**
    void *memset(void *ptr, int x, size_t n);

    set the next character in the storagebuffer to the value from read

    Parameters:
        x0 - pointer to current position in the storage buffer
        x1 - the value returned in the buffer from read
        x2 - the number of values to overwrite (1)

    Returns: 
        x0 - returns the pointer in x0 (what was passed as a parameter)
    */

    //increment current position in storagebuffer
    add x23, x23, 1
    //continue reading
    bl read_loop

newline:
    //this indicates whether the end of the file has been reached, it has not
    //so this is a 1
    mov x7, xzr
    mov x19, 1

end_of_file:
    //we need to set the last value in the memory to \0
    ldr x7, =storagebuffer
    cmp x23, x7
    bne not_end_of_file
    cbnz x19, not_end_of_file
    bl skip_creating_new_memory_for_end_of_file
    
not_end_of_file:
    mov x0, x23
    ldr x1, =nullchar
    ldr x1, [x1]
    mov x2, 1

    bl memset
    /*Documentation shown directly above*/

    //we need to create a new array to store the current line
    
    //the size of the array is the storage pointer - the beginning of the storage buffer
    ldr x18, =storagebuffer
    sub x0, x23, x18

    bl  malloc
    /*
    void * malloc(size_t size);

    allocates memory for the tail buffer for the exact size of the TAILLEN

    Parameters:
        x0 - the size of the allocated array, TAILLEN * (1 byte per char*)

    Returns:
        x0 - pointer to location allocated
    */

    //the new storage will be kept in x24 until it is moved to the tailbuffer
    mov x24, x0

    //now we loop through the storage buffer until we reach x23, setting memory along the way
    ldr x26, =storagebuffer
    mov x27, x24
    
    //x26 will go from the beginning of the buffer to the end. 
copy_from_storage_to_line_array:
    //aparently this could have been done with memcpy()

    //setup memset
    mov x0, x27     //the location to write
    ldr x1, [x26]   //the storage char
    mov x2, 1       //one byte

    bl memset

    //increment pointers
    add x26, x26, 1
    add x27, x27, 1

    //check if the end of the storage buffer has been reached.
    cmp x26, x23
    ble copy_from_storage_to_line_array

    /**
    *Here we are at line 67
    *I'm not sure if I've checked everything above
    *and I haven't done any debugging yet
    *so I don't know if any of this works...
    *
    *We're mostly done though! Only have one more check
    *todo, then printing out the tail array
    *
    *and then freeing everything
    *(free should work on null pointers, but if it doesn't
    * I will need to check)
    *
    *
    *
    *
    */

    //the storage pointer has reached the end, so we put the new memory
    //into the tail buffer

    //str 0, x22
    //ldrb w11, [x24]
    //strb w11, [x22]

    str x24, [x22]

    add x22, x22, 8

skip_creating_new_memory_for_end_of_file:
    //if the tail position has gone over the taillen, reset it and change x25 to 1
    //to indicate that the tailbuffer has been overwritten
    sub x10, x22, x21
    ldr x18, =TAILLEN
    mov x8, 8
    mul x18, x18, x8
    cmp x10, x18
    //only if x22-x21>=Taillen will it be overwritten
    blt skip_overwritten_buffer

    //indicate the buffer was overwritten
    mov x25, 1
    //reset the pointer
    mov x22, x21

skip_overwritten_buffer:

    //set the storage position to beginning of storagebuffer
    ldr x23, =storagebuffer

    //compare the result from read to 0, end of file
    cbnz x19, read_loop

    //if the tail buffer was overwritten, start at the current position, otherwise
    //start at the beginning of the tail buffer
    cbz x25, NOTcycledTailBufferLoopStart

cycledTailBufferLoopStart:

    //we need a counter for the loop
    mov x23, 0

cycledTailBufferLoop:

    //check to see if the position we're about to read from is over the 
    //limit of the tail buffer
    ldr x10, =TAILLEN
    mov x8, 8
    mul x10, x10, x8
    add x10, x10, x21
    cmp x22, x10
    bne skip_cycling_tail_buffer_in_read_loop

    //move the pointer to the start of the buffer
    mov x22, x21

skip_cycling_tail_buffer_in_read_loop:

    //set up printf
    ldr x0, =pstring
    ldr x1, [x22]
    bl printf


    //increment the counter and position in tailbuffer
    add x22, x22, 8
    add x23, x23, 1

    //if the counter has reached the limit, exit the loop
    ldr x18, =TAILLEN
    cmp x23, x18
    beq free_memory

    bl  cycledTailBufferLoop


NOTcycledTailBufferLoopStart:

    //set up a pointer for reading through tailbuffer
    //the storagebuffer pointer is no longer necessary, so using x23
    mov x23, x21

NOTcycledTailBufferLoop:

    ldr x0, =pstring
    ldr x1, [x23], 8
    bl  printf
    /*
    int printf(const char* format, char* string)

    prints the next line in the tail buffer

    Parameters:
        x0 - format string, "%s\n"
        x1 - value of string

    Returns:
        x0 - number of characters written

    */

    //increment the pointer (done above)
    //add x23, x23, 8

    //if the pointer is at the tailposition, end the print loop
    cmp x23, x22
    beq free_memory
    bl  NOTcycledTailBufferLoop

free_memory:/*
    //free the memory allocated for each of the pointers in tailbuffer
    //if the tailbuffer was overwritten, we can free everything
    cbnz x25, free_only_allocated_tail_buffer_lines
    //start at tailbuffer, go to tailbuffer+10
    ldr x18, =TAILLEN
    add x10, x21, x18
    mov x22, x10

    mov x23, x21

free_all_tail_buffer_lines_loop:
    //set up free
    ldr x0, [x23]
    bl  free
    /*  
    void free(void* ptr)

    frees the lines in tailbuffer

    Parameters:
        x0 - pointer to line from tailbuffer

    *//*

    //increment x23
    add x23, x23, 1

    //check if we've gone over the tailbuffer
    cmp x23, x22
    beq free_tail_buffer
    bl  free_all_tail_buffer_lines_loop



free_only_allocated_tail_buffer_lines:

    //we only need to go from x21 to x22
    mov x23, x21

free_only_allocated_tail_buffer_lines_loop:
    //set up free
    ldr x0, [x23]
    bl  free
    /* Function description shown directly above. *//*

    //increment the pointer
    add x23, x23, 1

    //check if we've gone up to the tailbuffer position
    cmp x23, x22
    beq free_tail_buffer
    bl  free_only_allocated_tail_buffer_lines_loop
*/
free_tail_buffer:
    //free the memory allocated for the tailbuffer
    mov x0, x21
    bl  free

    //no need to free the storagebuffer or readbuffer
    
code_exit_close_file:
    mov x0, x20
    bl  close
    /*
    int close(int fildes);
    x0 - file descriptor, retrieved from x20

    returns 
    x0 - -1 when an error occurs
    */

code_exit:
    ldp x25, x26, [sp], 32
    ldp x24, x20, [sp], 32
    ldp x21, x22, [sp], 32
    ldp x19, x23, [sp], 32
    ldp x30, x20, [sp], 32
    ret



    //errors handled below, if an error is thrown, just exit after appropriate actions are taken
err_usage:
    //prints the error message to the screen, then exits the program
    ldr x0, =pstring
    ldr x1, =usage
    bl  printf
    bl  code_exit
err_badopen:
    //calls perror, which prints to the screen, then exits the program
    ldr x0, =badopen
    bl  perror
    bl  code_exit
err_noline:
    //similar to err_usage
    ldr x0, =pstring
    ldr x1, =noline
    bl  printf
    bl  code_exit
err_badtail:
    //similar to err_usage
    ldr x0, =pstring
    ldr x1, =badtail
    bl  printf
    //there is a file open, so close it
    bl  free_memory
err_badalloc:
    //similar to err_usage
    ldr x0, =pstring
    ldr x1, =badalloc
    bl  printf
    bl  code_exit

    .data
    readbuffer:     .space  1 //space for the read function
    storagebuffer:  .space  4096
    usage:		.asciz	"File name must be given."
    badopen:	.asciz	"Open file failed"
    noline:		.asciz	"Allocating line buffer failed."
    badtail:	.asciz	"Allocating tail pointer buffer failed."
    badalloc:	.asciz	"Allocating a tail line failed."
    badalloc_misc: .asciz "Allocating miscellaneous memory failed."
    pstring:    .asciz  "%s\n"
    newlinechar: .asciz "\n"
    nullchar: .asciz "\0"

    .end

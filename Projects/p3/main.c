#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char* argv[]){
    int taillen = 3;
    //check for file given
    if(argv[1]==0){
        printf("%s\n","Please provide a file to open.");
        return;
    }
    int fd = open(argv[1],0);
    //check for valid file
    if(fd==-1){
        printf("%s\n","This file could not be opened.");
        return;
    }

    //allocate memory for tailbuffer
    char** tailbuf = (char**) malloc(taillen);
    //tailbuf == x21
    char** tailpos = tailbuf;
    //tailpos == x22

    //allocate memory for storagebuffer
    char* storagebuffer = (char*) malloc(4096);
    char* storagepos = storagebuffer;
    //storagepos == x23

    //allocate memory for readbuffer
    char* readbuf = (char*) malloc(1);
    *readbuf = '0';

    int cycledTailBuffer = 0;
    printf("%s","starting loop\n");

    //loop through the line
    while(1==1){
        //read one character at a time, putting them into readbuf
        int result = read(fd, readbuf, 1);
        //printf("%s",*readbuf);
        //if we haven't reached a newline or terminating character
        if(*readbuf != '\n' && result != 0){
            //if we are not over the limit of the storagebuffer
            if(storagepos-storagebuffer<4095){
                //put the next character in the buffer and move on
                *storagepos=*readbuf;
                storagepos++;
            }
        }
        else {
            //if we have reached a newline or the end of the file, 
            //we need to save the current line into its position
            //set the last character to '\0' and increment for the correct size
            *storagepos = '\0';
            storagepos++;

            //allocate memory for the string to be pointed to in tailbuffer
            char* tmp = malloc(storagepos-storagebuffer);
            //and a pointer to set the new array with data
            char* qtr = tmp;
            //loop through both arrays and set the data from storage into the new linebuffer
            for(char* ptr = storagebuffer;ptr!=storagepos;ptr++){
                *qtr = *ptr;
                qtr++;
            }
            //now the temp buffer has the correct line in it, put it into tailbuffer
            if(tailpos!=nullptr){
                free(tailpos);
            }
            *tailpos = tmp;
            tailpos++;
            if(tailpos-tailbuf>=taillen){
                tailpos = tailbuf;
                cycledTailBuffer = 1;
            }
            storagepos = storagebuffer;

            //if we reached the end of the file, print out all of the buffers 
            if(result == 0){
                if(cycledTailBuffer==1){
                    for(int i = 0;i<taillen;i++){
                        if(tailpos-tailbuf>=taillen){
                            tailpos = tailbuf;
                        }
                        printf("%s\n",*tailpos);
                        tailpos++;
                    }
                }
                else{
                    for(char** ptr = tailbuf;ptr!=tailpos;ptr++){
                        printf("%s\n",*ptr);
                    }
                }
                break;
            }
        }
    }

    for(tailpos = tailbuf;tailpos-tailbuf<taillen;tailpos++){
        free(*tailpos);
    }


    free(tailbuf);
    free(readbuf);
    free(storagebuffer);
}
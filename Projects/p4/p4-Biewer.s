.text
        .align 2
        .global main

    /*  Registers:
    x9-15, temporary variables, don't need to be saved
    
    d8 - two*pi
    d9 - temporary variables
    d10 - phase
    d11 - theta
    d12 - increment
    w23 - l (line counter)
    w24 - c (column counter)
    w25 - intensity
    */

main:       
        stp x30, x23, [sp, -16]!
        stp x24, x25, [sp, -16]!
        stp d8, d9, [sp, -16]!
        stp d10, d11, [sp, -16]!
        stp d12, d13, [sp, -16]!

        //initscr();
        bl initscr
        
        //float tpi = pi * 2;
        ldr d0, pi
        //ldr d8, [x10]
        fadd d8, d0, d0

        //float phase = 0;
        //signed convert to float
        scvtf d9, xzr
        fmov d10, d9

        //float theta = 0;
        scvtf d9, xzr
        fmov d11, d9

        //(float)COLS
        ldr x10, =COLS
        ldr x10, [x10]
        scvtf d9, x10
        //float increment = tpi/(float)COLS;
        fdiv d12, d8, d9

top:
        //erase();
        bl  erase

        //phase += increment;
        fadd d10, d10, d12

        //l = 0;
        mov w23, wzr

sinner:
        //if(l>=LINES) goto bottom;
        ldr x10, =LINES
        ldr x10, [x10]
        cmp w23, w10
        bge bottom

        //theta = 0;
        scvtf d9, xzr
        fmov d11, d9

        //c = 0;
        mov w24, wzr

tinner:
        //if(c>=COLS) goto binner
        ldr x10, =COLS
        ldr x10, [x10]
        cmp w24, w10
        bge binner

        //int intensity = (int) ( ( sin(phase + theta) + 1.0) * 5.0);
        fadd d0, d10, d11
        bl  sin
        fmov d9, 1.0
        fadd d0, d0, d9
        fmov d9, 5.0
        fmul d0, d0, d9
        fcvtms w25, d0

        //mvaddch(l, c, levels[intensity]);
        mov w0, w23
        mov w1, w24
        ldr w2, =levels
        add w2, w2, w25
        ldrb w2, [x2]
        bl  mvaddch

        //theta += increment;
        fadd d11, d11, d12

        //c++
        add w24, w24, 1

        //goto tinner;
        b   tinner
        
binner:
        //l++;
        add w23, w23, 1
        //goto sinner
        b   sinner

bottom:
        //box(stdscr,0,0);
        ldr x0, =stdscr
        ldr x0, [x0]
        mov x1, 0
        mov x2, 0
        bl  box

        //refresh();
        bl  refresh

        //goto top;
        b   top

        bl  endwin

        ldp d12, d13, [sp], 16
        ldp d10, d11, [sp], 16
        ldp d8, d9, [sp], 16
        ldp x24, x25, [sp], 16
        ldp x30, x23, [sp], 16
        mov x0, xzr
        ret

        .data
        .align 2
        pi:     .double 3.14159265359
        levels: .asciz  " .:-=+*#%@"
        .end

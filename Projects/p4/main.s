        .text
        .align 2
        .global main

    /*  Registers:
    x9-15, temporary variables, don't need to be saved
    
    d19 - two*pi
    d20 - phase
    d21 - theta
    d22 - increment
    w23 - l (line counter)
    w24 - c (column counter)
    w25 - intensity
    */

main:       
        stp x30, x19, [sp, -16]!
        stp x20, x21, [sp, -16]!
        stp x22, x23, [sp, -16]!
        stp x24, x25, [sp, -16]!

        //initscr();
        bl initscr
        
        //float tpi = pi*2;
        ldr d19, =pi
        fadd d19, d19, d19
debug:
        //debug TODO:
        ldr x0, =printdouble
        fmov d1, d19

        bl  printf
        /*
        //float phase = 0;
        scvtf d9, xzr
        fmov d20, d9
        //float theta = 0;
        fmov d21, d9

        //float increment = tpi/(float) COLS;
        //signed-convert-to-float COLS
        /*ldr w10, =COLS
        scvtf d9, x10
        fdiv d22, d19, d9*/
/*
top:
        //erase();
        bl erase
        //phase += increment;
        /*fadd d20, d20, d22*/
        //l = 0;
        /*mov w23, wzr*/
/*
sinner:
        //if(l>=LINES) goto bottom;
        ldr x10, =LINES
        cmp w23, w10
        bge bottom
        //theta = 0;
        scvtf d9, xzr
        fmov d21, d9
        //c = 0;
        mov w24, wzr
        
tinner:
        //if(c>=COLS) goto binner;
        ldr x10, =COLS
        cmp w24, w10
        bge binner
        //int intensity = (int) ( (sin(phase + theta) + 1.0) / 2.0 * 10)

        //phase+theta
        fadd d10, d20, d21
        fmov d0, d10
        bl  sin

        fmov d10, 1.0
        fadd d0, d0, d10
        
        fmov d10, 2.0
        fdiv d0, d0, d10

        fmov d10, 10.0
        fmul d0, d0, d10

        //float convert towards minus signed
        mov x25, xzr
        fcvtms w25, d0

        mov w0, w23
        mov w1, w24
        ldr x2, levels
        add x2, x2, x25
        ldr x2, [x2]

        bl mvaddch*/
/*
        fadd d21, d21, d22
        add w24, w24, 1
        b   tinner
*/
//binner:
/*
        add w23, w23, 1
        b   sinner
*/
//bottom:
        //ldr x0, =stdscr
        //mov x1, xzr
        //mov x2, xzr
        //bl  box
        //bl  refresh
        //b   top
        
        bl  endwin

        ldp x24, x25, [sp], 16
        ldp x22, x23, [sp], 16
        ldp x20, x21, [sp], 16
        ldp x30, x19, [sp], 16
        mov x0, xzr
        ret

        .data
        levels: .asciz  " .:-=+*#%@"
        pi:     .double 3.14159265359
        printdouble: .asciz "%d\n"
        .end

# Crusade Against (File System) Corruption

In this project you will be given one or more xv6 file systems which may or may not have been corrupted in different ways. Your job is to write an application which reads a file system file and parses the file system contained therein to locate every instance of corruption, describing each fully in print outs.

You are *not* responsible for repairing the corruption you find.

# Final project

This is now your final exam - it carries more weight than other projects. In the end you will have had many weeks to do this project as the work / experience from the previous attempt can be reused. The project will, accordingly, be graded harshly so do your best work.

# Overall layout of the file system

The overall layout is:

```
boot block | super block | log | inode blocks | free bit map | data blocks
```

The boot block is present but not used in this project.

Log blocks are present but not used in this project.

Every block is 512 bytes (```BSIZE```).

The Super Block is in block 1. It's structure is:

```c
struct superblock {
  uint size;         // Size of file system image (blocks)
  uint nblocks;      // Number of data blocks
  uint ninodes;      // Number of inodes.
  uint nlog;         // Number of log blocks
  uint logstart;     // Block number of first log block
  uint inodestart;   // Block number of first inode block
  uint bmapstart;    // Block number of first free map block
};
```

Of course, you should check this in ```fs.h``` in case this document is not current.

You will ignore the log blocks.

# fs.h

Nearly everything you need to known about the file system is contained here:

[fs.h](./fs.h)

There are a number of handy C macros in this file. For example, ```IPB``` will compute the number of blocks that some specified number of inodes will consume.

## The superblock and the first thing you should do

The super block contains items that will help you find various parts of the file system.

The first thing you should write is code to check the size of the file system file against the ```size``` in the super block. Ensure you can make sense of the ```size``` field before you move on.

You must print out every member of the superblock. During the checking your program will perform, the values in the superblock should be checked to the degree possible. For example, you can check ```size```. You can figure out if some other fields are correct. for example, you have ```bmapstart``` and can figure out how many bitmap blocks are appropriate. From this you can figure out where the data blocks start and from this you can check ```nblocks```.

## Inodes

The file sysem's inodes are simplified from those we learned about.

For you, this is a *Good Thing*.

![file system](./F6-4.png)

Notice there is only a single layer of indirection possible in this file system.

For you, this is a *Good Thing*.

Ignore ```major``` and ```minor```.

## No free block list

This file system does away with the free block list. 

For you, this is a *Good Thing*.

Instead it keeps track of free blocks using a bit map. One bit corresponds to one block. A 512 byte block contains 4096 bits. Therefore a single block is needed for the file systems you will be given (as they contain fewer than 4096 blocks).

# types.h

Self explanatory.

# stat.h

From here you get the definition of ```T_DIR``` and ```T_FILE``` and ```T_DEV```. Pull these values out as there is a standard ```stat``` Unix function call and structure with the same same.

# Build an in-memory model of the file system

You must build an in-memory model of the file system so that you can account for every file, block, inode, directory etc. This does *not* mean you simply read the whole file system file into memory.

With regard to reading the entire file system image into memory, this is a *Good Thing*. Having the entire file system image in memory will make making your own model much easier. You can do this in two ways (at least). You can use ```read()``` of course after allocating the right number of bytes for your buffer. 

You can alternatively use ```mmap()```. ```mmap()``` is cool in that it lets the OS read the whole file into memory which the OS manages and maps the buffer into your address space.

# Address arithmetic

Experience shows that you will screw up address arithmethic and therefore will not be able to do your work. Hint - ```malloc``` returns ```void *```. This is a pain-in-the-ass to do address arithmetic with. Why not type your buffer as something more convenient and cast the return value of ```malloc``` to the convenient type making all future address arithmetic easier.

# Types of errors

The following types of corruption will be present in one or more of the file system files you are given. To make things extra rigorous for you, not all of these errors are present in the file system images you are given.

Also, some number of file system images contain no error at all.

## Inode 0 seems to be allocated

The inode at index 0 must not have a ```type``` as it is used in directories to indicate an available ```dirent```. 

## Missing block

The bit map says block *n* is allocated but you cannot find it belonging to  any file or directory.

## Unallocated block

The bit map says block *n* is free but you found it referenced by a valid inode.

## Multiply allocated block

An allocated block is referenced by more than one inode, or more than once by the same inode.

## Missing inode

An inode number is found in a valid directory but is not marked as allocated in the inode array.

## Unused inode

An inode marked valid is not referred to by a directory.

## / problems

These problems relate to /.

- Inode 1 is not allocated.

- Inode 1 is not a directory.

- The root directory does not refer to itself as . and ..

## . and ..

A directory's first two file names are not . and ..

## A directory appearing more than once not counting . and ..

This file system does not allow links to directories. This error condition is easier to detect than the one previously contained here: directory link loops. For you this is a *Good Thing*.

## Superblock problems

As indicated above, sanity check what you can in the superblock.

## A . or .. inode is not a directory

The inode numbers mapping a . or .. lead to non-directories. 

## A non-root directory with . listing inode 1

Inode 1 is the root directory. No other directory can list itself (i.e. .) as inode 1.

## Directory link counts

The link count in a directory inode is not the number of directories it contains. Note this is really covered by the next error.

## File link count

The link count of a file is not the number of times the file is referred to by a directory.

## File size

A file's size in bytes cannot exceed the maximum file size (figure out what this is). Or, the size doesn't fit with the number of blocks the inode is using.

# Platform and language

All work must be in C and / or C++ designed for Linux and compilable by ```g++ -Wall -g -std=c++11```. Your work will be graded on an Ubuntu Linux system of at least version 16.

# Usage

Your program should check the validity of ```argv[1]```. It is an error should the argument not be given or is bogus.

# All error messages 

Should be as specific as possible listing block or inode numbers.

# Program return

Return 0 for good file systems. Return 1 for bad file systems.

# Test images

You are given a zip file with 16 test images and one known good image. They each contain 0 or 1 errors. As indicated, not every error state indicated above is represented. Wouldn't it be sweet if you were told which ones? Yeah, well.

Since you are given a promise that a file system image will contain at most 1 error, you can exit after finding 1 error (and cleaning up all allocated resources).

# Easy ways to lose points

You are Juniors and Seniors nearly ready to work on Boeing 737-Max-8's. Easy ways to lose points include (but is not limited to):

- Even a single warning of any kind loses 10 points.

- Failing to deallocate an allocated resources loses 10 points. This includes files not being closed, dynamically allocated memory not being free.

- Any crash will either lose 100 points if it is something that can be construed as you not having tested your code.

- Failing to compile loses 100 points. How minor the problem is doesn't matter. There is no excuse for handing code that doesn't compile.

- Failing to hand in work loses 100 points.

- Infinite loop loses 100 points.

- Failure to produce correct results even once loses *at least* 10 points.

# Expectation

You are expected to have tested with **every file system image.**

# Something new - READ THIS

No late work. Anything submitted past the due date and time will not be graded.

# Partner rules

All work is **solo**.

**No partners.**





#include "fs.h"
#include "types.h"
#include "fileSystemStruct.h"

#include <iostream>
#include <set>
#include <fstream>


using namespace std;

int main(int argc, char* argv[]){
    //check to make sure there is a valid file
    if(argc<2)
    {
        cout<<"Invalid number of arguments"<<endl;
        return 1;
    }

    string ALL = "ALL";
    if(ALL==argv[1]){
        for(int i = 1;i<17;i++){
            cout<<"\nFile: " << i << "------------"<<endl;
            string filename = "images/"+to_string(i)+".img";

            //open the next image file
            ifstream fileSystemImage;
            fileSystemImage.open(filename);

            //make sure the file was opened
            if(!fileSystemImage.is_open())
            {
                cout<<"Invalid file name"<<endl;
                continue;
            }

            //give the file system to the struct, meta data is checked in the constructor
            fileSystemStruct fileSystem(fileSystemImage);

            if(!fileSystem.fileSystemOK){
                continue;
            }

            if(fileSystem.checkInodeZeroAndRoot()){
                continue;
            }

            if(fileSystem.checkDirectories()){
                continue;
            }

            fileSystemImage.close();
        }
    }
    else{
        //open the file in argv[1]
        ifstream fileSystemImage;
        fileSystemImage.open(argv[1]);

        //make sure the file was opened
        if(!fileSystemImage.is_open())
        {
            cout<<"Invalid file name"<<endl;
            return 1;
        }

        //give the file system to the struct, meta data is checked in the constructor
        fileSystemStruct fileSystem(fileSystemImage);

        if(!fileSystem.fileSystemOK){
            return 1;
        }

        if(fileSystem.checkInodeZeroAndRoot()){
            fileSystemImage.close();
            return 1;
        }

        if(fileSystem.checkDirectories()){
            fileSystemImage.close();
            return 1;
        }

        fileSystemImage.close();
    }

    return 0;
}
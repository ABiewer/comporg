#include "fileSystemStruct.h"
#include "types.h"

#include <iostream>
#include <math.h>

using namespace std;

bool sizeCheck(uint nData, uint size, uint fileINum);

//superblock* superptr;
//uint fileSize;
//uchar* allInodes;
//vector<bool> bitmap;
//uchar* allDataBlocks;

fileSystemStruct::fileSystemStruct(ifstream& fileSystemImage)
{
    fileSystemOK = true;
    superptr = NULL;
    allDataBlocks = NULL;

    //superBuff[BSIZE*2];
    //get a pointer to the superblock's info
    superptr = (superblock*)(superBuff+BSIZE);
    fileSystemImage.read(superBuff, BSIZE*2);

    //print out the meta data
    cout<<"Size: " << superptr->size<<endl;
    cout<<"Number of data blocks: " <<superptr->nblocks<<endl;
    cout<<"Number of inodes: "<<superptr->ninodes<<endl;
    cout<<"Number of log blocks: " << superptr->nlog <<"\n\n--------------------------\n"<<endl;

    //get the file system size, to check again  st superblock size
    fileSystemImage.seekg(0, fileSystemImage.end);
    fileSize = (uint)fileSystemImage.tellg();

    //make sure the file sizes match
    if((superptr->size)*BSIZE!=fileSize)
    {
        cout<<"The size given in the super block: "<<superptr->size<<" does not match the size of the file: "<<fileSize/BSIZE<<endl;
        fileSystemOK = false;
        return;
    }


    //get all of the inodes into memory
    dinode* allInodesPtr = (dinode*)malloc(superptr->ninodes * sizeof(dinode));
    fileSystemImage.seekg(BSIZE * (2+superptr->nlog), fileSystemImage.beg);
    //have an extra inode (0)
    fileSystemImage.read((char *)allInodesPtr, sizeof(dinode) * (superptr->ninodes));
    
    dinode* qtr = allInodesPtr;
    for(uint i = 0;i<superptr->ninodes;i++){
        allInodes.push_back(*qtr);
        qtr++;
    }

    free((void*) allInodesPtr);


    //get the number of blocks the inodes take up
    ninodeBlocks = (uint)ceil((double)sizeof(dinode)*((double)superptr->ninodes+1)/(double)BSIZE);
    //I am making the assumption here that inode 0 is not counted by the number of available inodes in the super block
    //if that's not the case, then i don't know why there is one extra block after the inodes, before the bitmap

    //get the bitmap into memory
    uint seekDistance = BSIZE * (2+superptr->nlog+ninodeBlocks);
    fileSystemImage.seekg(seekDistance, fileSystemImage.beg);
    uint nbitmapBlocks = (uint)ceil((double)superptr->nblocks/(double)(BSIZE*8));
    uchar * bitmapBlocks = (uchar*)malloc(nbitmapBlocks*BSIZE);
    fileSystemImage.read((char*)bitmapBlocks,BSIZE*nbitmapBlocks);
    
    //sort the bitmap into a vector for easy access
    uchar* ptr = bitmapBlocks-1;
    for(uint i = 0;i<superptr->nblocks;i++){
        uint bitNumber = i%8;
        if(bitNumber==0){
            ptr++;
        }
        uchar byte = *ptr;
        byte = (byte<<bitNumber)>>(7);
        bitmap.push_back((bool)byte);
    }
    free(bitmapBlocks);

    //get the data blocks
    //uchar* allDataBlocks;
    seekDistance = BSIZE * (2+superptr->nlog+ninodeBlocks + nbitmapBlocks);
    fileSystemImage.seekg(seekDistance, fileSystemImage.beg);
    allDataBlocks = (uchar*)malloc(superptr->nblocks*BSIZE);
    fileSystemImage.read((char*)allDataBlocks, superptr->nblocks*BSIZE);

    //time to check meta data

    //starting with all the allocated blocks
    // cout<<ninodeBlocks<<endl;
    uint nUsedBlocks = superptr->nblocks+superptr->nlog+ninodeBlocks+1+1+nbitmapBlocks;
    if(nUsedBlocks != superptr->size){
        cout<<"The number of blocks used by the file system: "<<nUsedBlocks<<" does not match the file system size: "<<superptr->size<<endl;
        fileSystemOK = false;
        return;
    }
    // apparently, these numbers don't match even in the good file. Shrug

    //location of log blocks
    if(superptr->logstart!=2){
        cout<<"The log starts at block: "<<superptr->logstart<<" but should start at block 2."<<endl;
        fileSystemOK= false;
        return;
    }

    //location of inodes
    uint inodeStart = 2+superptr->nlog;
    if(inodeStart!=superptr->inodestart){
        cout<<"The start of the inode blocks: "<<inodeStart<<" does not match the starting location in the super block: "<<superptr->inodestart<<endl;
        fileSystemOK = false;
        return;
    }

    //location of bitmap
    uint bitmapStart = inodeStart + ninodeBlocks;
    if(bitmapStart!=superptr->bmapstart){
        cout<<"The start of the bitmap blocks: "<<bitmapStart<<" does not match the starting location in the super block: "<<superptr->bmapstart<<endl;
        fileSystemOK = false;
        return;
    }
}

fileSystemStruct::~fileSystemStruct(){
    if(allDataBlocks!=NULL){
        free((void*)allDataBlocks);
    }
}

uchar* fileSystemStruct::getDataBlock(uint number){
    uchar* retval = (uchar*)allDataBlocks;
    uint offset = (uint)(2+ninodeBlocks+superptr->nlog + 1);
    if(number<offset || number>superptr->nblocks){
        throw std::out_of_range("Invalid data block number: " + number);
    }
    retval += (number-offset)*BSIZE;
    return retval;
}

bool fileSystemStruct::checkInodeZeroAndRoot(){
    //make sure inode 0 is not allocated
    if(allInodes[0].type!=0){
        cout<<"Inode 0 has the type: "<<allInodes[0].type<<" when it should be unallocated"<<endl;
        return 1;
    }

    //make sure '.' and '..' in inode 1 are reference to inode 1
    dinode root = allInodes[1];
    if(root.type!=T_DIR){
        cout<<"The root inode has type: "<<root.type<<" when it should have type: 1"<<endl;
        return 1;
    }
    dirent* entry;
    try{
        entry = (dirent*)getDataBlock(root.addrs[0]);
    }
    catch(out_of_range orr){
        cout<<"Index out of bounds: "<<orr.what()<<endl;
        return 1;
    }
    string dot = ".";
    string dotdot = "..";

    if( entry->inum !=1 ){
        cout<<"Root's '.' does not refer to itself as 1, instead: "<<entry->inum<<endl;
        return 1;
    }
    if(dot!=entry->name){
        cout<<"Root does not refer to itself as '.', instead: "<<entry->name<<endl;
        return 1;
    }

    entry++;

    if( entry->inum !=1 ){
        cout<<"Root's '..' does not refer to itself as 1, instead: "<<entry->inum<<endl;
        return 1;
    }
    if(dotdot!=entry->name){
        cout<<"Root does not refer to itself as '..', instead: "<<entry->name<<endl;
        return 1;
    }

    return 0;
}

bool fileSystemStruct::checkDirectories(){
    //make sure the first two entries in a directory are '.' and '..'
    //make sure . and .. refer to itself and its parent
    //make sure there are no duplicate directories

    //get the root inode
    dinode root = allInodes.at(1);
    //add it to the used Inodes array
    usedInodeNums.push_back(1);
    //count the number of links to itself
    claimedLinks[1]=root.nlink;
    actualLinks[1]=0;

    //for each direct address in the root inode
    for(uint i = 0;i<NDIRECT;i++){
        //the first two inodes should be checked by checkInodeZeroAndRoot();
        //otherwise, if the direct address points to no data block, we win
        if(root.addrs[i]==0){
            //reached the end of the file system
            return 0;
        }
        try{
            //in the first direct address data block,
            dirent* entry = (dirent*)getDataBlock(root.addrs[i]);
            //check each entry for multiple things:
            //  1: first entry is '.', second entry is '..' (already checked for root)
            //  2: '.' and '..' reference the correct inode numbers, already checked for root
            //  3: increase the link count of that directory entry
            //  4: follow that directory entry to its inode and recursively check for duplicates
            for(ushort k = 0;k<BSIZE/sizeof(dirent);k++){
                if(i==0){//root
                    //parts 1, 2, and 3
                    entry+=2;
                    actualLinks[1] += 2;
                    k+=2;
                }
                if(entry->inum == 0){
                    break;
                }

                //4: begin the recursive process of checking every inode for duplicates and link counts
                if(recursiveCheckDirectories(entry->inum, 1)){
                    return 1;
                }
                entry++;
            }
        }
        catch(out_of_range orr){
            cout<<"Index out of bounds: "<<orr.what()<<endl;
            return 1;
        }

    }

    //check the nlinks


    return 0;
}

bool fileSystemStruct::recursiveCheckDirectories(ushort inodeNum, ushort parentNum){
    //for each inode, apart from their data, each needs to be checked for:
    //  0: it is not a duplicate directory in the system (duplicate links to files are fine)
    //  1: nlink gets added
    //  2: check the size of the file matches the number of data blocks it has

    dinode thisInode = allInodes.at(inodeNum);
    dinode parentInode = allInodes.at(parentNum);
    uint numBlocksUsed = 0;
    // this inode is used

    //0: check for duplicate directories
    for(size_t i= 0;i<usedInodeNums.size();i++){
        if(usedInodeNums.at(i)==inodeNum){
            //and it has been found multiple times
            if(thisInode.type==T_DIR){
                cout<<"The directory: "<<inodeNum<<" has been referenced at least twice."<<endl;
                return 1;
            }
        }
    }
    usedInodeNums.push_back(inodeNum);

    //1: add the nlink to the map
    claimedLinks[inodeNum] = thisInode.nlink;

    if(thisInode.type != T_DIR){
        //do file size checking
        if(checkFileSize(inodeNum)){
            return 1;
        }
        return 0;
    }

    //check each entry for multiple things:
    //  --don't have to do this.......3: check to make sure the ascii values in the name are all correct
    //  4: first entry is '.', second entry is '..'
    //  5: '.' and '..' reference the correct inode numbers
    //  6: increase the link count of each directory entry
    //  7: follow that directory entry to its inode and recursively check for duplicates

    for(uint i = 0;i<NDIRECT;i++){
        dirent* entry = (dirent*)getDataBlock(thisInode.addrs[i]);
        numBlocksUsed++;
        for(ushort k = 0;k<BSIZE/sizeof(dirent);k++){
            if(entry->inum==0 && (i>0||k>2)){ //not the first 2 entries
                return 0;
            }

            //4, 5: if it's the first entry, inum better be inodeNum, and the name is '.'
            if(i == 0 && k==0){
                if(entry->inum!=inodeNum){
                    cout<<"Inode: "<<inodeNum<<"'s first entry in the directory does not reference itself. Instead: "<<entry->inum<<endl;
                    return 1;
                }
                string dot = ".";
                if(dot!=entry->name){
                    cout<<"Inode: "<<inodeNum<<"'s first entry in the directory is called: "<<entry->name<<endl;
                    return 1;
                }
            }
            //4, 5: if it's the second entry, inum better be the parentNum, and the name is '..'
            if(i==0 && k==1){
                if(entry->inum!=parentNum){
                    cout<<"Inode: "<<inodeNum<<"'s second entry references inode: "<<entry->inum<<" when it should reference its parent, inode: "<<parentNum<<endl;
                    return 1;
                }
                string dotdot = "..";
                if(dotdot!=entry->name){
                    cout<<"Inode: "<<inodeNum<<"'s second entry is called: "<<entry->name<<endl;
                    return 1;
                }
            }

            //6: increase the link count to the file 
            actualLinks[entry->inum]++;

            //7: do recursive tree searching
            if(k>1){ //don't do recursion on '.' and '..'
                if(recursiveCheckDirectories(entry->inum, inodeNum)){
                    return 1;
                }
            }
            entry++;
        }
    }

    //file size checking


    return 0;
}

bool fileSystemStruct::checkFileSize(uint inodeNum){
    //TODO: check this for file 15, weird thing with file size

    dinode thisInode = allInodes.at(inodeNum);
    uint numDataBlocks = 0;
    
    //direct
    for(uint i = 0;i<NDIRECT;i++){
        //
        if(thisInode.addrs[i]==0){
            if(sizeCheck(numDataBlocks, thisInode.size, inodeNum)){
                return 1;
            }
            return 0;
        }
        numDataBlocks++;
    }

    //indirect
    if(thisInode.addrs[NDIRECT]==0){
        //
        if(sizeCheck(numDataBlocks, thisInode.size,inodeNum)){
            return 1;
        }
        return 0;
    }
    uint* entry= (uint*)getDataBlock(thisInode.addrs[NDIRECT]);
    for(uint i = 0;i<BSIZE/sizeof(uint);i++){
        if(*entry==0){
            break;
        }
        else{
            entry++;
            numDataBlocks++;
        }
    }

    if(sizeCheck(numDataBlocks, thisInode.size, inodeNum)){
        return 1;
    }
    return 0;
}

bool sizeCheck(uint nData, uint size, uint fileINum){
    nData = nData*BSIZE;
    if(size<=nData && (size+BSIZE)>=nData){
        return 0;
    }
    else{
        cout<<"The file: "<<fileINum<<" has a size: "<<size<<" but does not fit into: "<<nData<<" data blocks (size: "<<nData*BSIZE<<")"<<endl;
        return 1;
    }
}
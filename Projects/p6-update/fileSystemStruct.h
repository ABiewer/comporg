#ifndef _fileSystemStruct_
#define _fileSystemStruct_

#include "types.h"
#include "fs.h"
#include <vector>
#include <string>
#include <fstream>
#include <map>

using namespace std;


// In-memory file system format.
struct fileSystemStruct{
    superblock* superptr;
    vector<dinode> allInodes;
    vector<uint> usedInodeNums;
    vector<uint> usedDataBlocks;
    //uchar* allInodes;
    const uchar* allDataBlocks;
    vector<bool> bitmap;
    char superBuff[BSIZE*2];
    uint fileSize;
    uint ninodeBlocks;
    bool fileSystemOK;
    map<uint,uint> claimedLinks;
    map<uint,uint> actualLinks;

    fileSystemStruct(ifstream& fileSystemImage);
    ~fileSystemStruct();

    uchar* getDataBlock(uint number);

    bool checkInodeZeroAndRoot();
    bool checkDirectories();
    bool recursiveCheckDirectories(ushort inodeNum, ushort parentNum);
    bool checkFileSize(uint inodeNum);
};



#endif // _fileSystemStruct_
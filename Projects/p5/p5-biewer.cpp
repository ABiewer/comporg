#include <iostream>
#include <getopt.h>
#include <cstdlib>
#include <iomanip>
#include <cassert>
#include <arm_neon.h>
#include <vector>
#include <thread>
#include <sys/time.h>
#include <cstring>

using namespace std;

void do_join(thread& t);
void join_all(vector<thread>& v);

void setArrayToValue(float *c, float val, size_t size);
void setRandomValues(float* a, int size);

int parseArgs(int argc, char **argv, int &size, int &iter);
float SumOfSums(float *a, int size);

void SingleCore(float *a, float *b, float *c, int size);
void Intrinsic(float * a, float * b, float * c, int size);

void MultiCore(float *a, float *b, float *c, int size, int numCores);
void MultiIntrinsic(float * a, float * b, float * c, int size, int numCores);

int main(int argc, char *argv[])
{
    int givenSize = 128;
    int arraySize = 128;
    int iter = 1;
    int numCores = thread::hardware_concurrency();

    //figures out which command line argument options are given and sets the size/iterations
    if (parseArgs(argc, argv, givenSize, iter) != 0)
        return 1;

    arraySize = givenSize;

    //makes sure the size is a multiple of 16
    if (arraySize % 16 != 0)
        arraySize += 16 - arraySize % 16;
    
    //time values to calculate the amount of time spent
    struct timeval start, end;
    gettimeofday(&start, NULL);

    //random number seeder
    srand(start.tv_usec);
    //srand(100);

    //The arrays for the computations
    float *a = (float *)aligned_alloc(16, (size_t)(arraySize * sizeof(float)));
    float *b = (float *)aligned_alloc(16, (size_t)(arraySize * sizeof(float)));
    float *c = (float *)aligned_alloc(16, (size_t)(arraySize * sizeof(float)));

    //set the values in arrays a, b to random numbers
    setRandomValues(a, arraySize);
    setRandomValues(b, arraySize);

    //info from all runs
    cout << "Array size: " << givenSize;
    cout << " total size in MB: " << (float)(givenSize * sizeof(float)) / (float)(1024 * 1024) << endl;
    cout << "Iterations: " << iter << endl;

    //indicator for single/multi core
    cout << "Single core timings..." << endl;



    //timers for the program
    gettimeofday(&start, NULL);

    for (int i = 0; i < iter; i++)
    {
        //reset the sum array
        setArrayToValue(c, (float)0, arraySize);
        //calculate the values with one core
        SingleCore(a, b, c, arraySize);
    }

    //time the computations
    gettimeofday(&end, NULL);

    //print out the results for the single core
    cout << "Naive: " << ((double)(end.tv_sec * 1000000 + end.tv_usec) - (double)(start.tv_sec * 1000000 + start.tv_usec)) / (double)(iter) / (double)(1000000);
    cout << "s Check: " << SumOfSums(c, givenSize) << endl;



    //timers for the program
    gettimeofday(&start, NULL);

    for (int i = 0; i < iter; i++)
    {
        //reset the sum array
        setArrayToValue(c, (float)0, arraySize);
        //calculate the values with one core
        Intrinsic(a, b, c, arraySize);
    }

    //time the computations
    gettimeofday(&end, NULL);

    //print out the results for the single core
    cout << "NEON:  " << ((double)(end.tv_sec * 1000000 + end.tv_usec) - (double)(start.tv_sec * 1000000 + start.tv_usec)) / (double)(iter) / (double)(1000000);
    cout << "s Check: " << SumOfSums(c, givenSize) << endl;



    //now we do the multi-core
    cout<<"Threaded timing..."<<endl;
    cout<<"Number of cores: "<<numCores<<endl; 



    //timers for the program
    gettimeofday(&start, NULL);

    for (int i = 0; i < iter; i++)
    {
        //reset the sum array
        setArrayToValue(c, (float)0, arraySize);
        //calculate the values with one core
        MultiCore(a, b, c, arraySize, numCores);
    }

    //time the computations
    gettimeofday(&end, NULL);

    //print out the results for the single core
    cout << "Naive: " << ((double)(end.tv_sec * 1000000 + end.tv_usec) - (double)(start.tv_sec * 1000000 + start.tv_usec)) / (double)(iter) / (double)(1000000);
    cout << "s Check: " << SumOfSums(c, givenSize) << endl;



    //timers for the program
    gettimeofday(&start, NULL);

    for (int i = 0; i < iter; i++)
    {
        //reset the sum array
        setArrayToValue(c, (float)0, arraySize);
        //calculate the values with one core
        MultiIntrinsic(a, b, c, arraySize, numCores);
    }

    //time the computations
    gettimeofday(&end, NULL);

    //print out the results for the single core
    cout << "NEON:  " << ((double)(end.tv_sec * 1000000 + end.tv_usec) - (double)(start.tv_sec * 1000000 + start.tv_usec)) / (double)(iter) / (double)(1000000);
    cout << "s Check: " << SumOfSums(c, givenSize) << endl;



    //free the arrays
    free(a);
    free(b);
    free(c);

    return 0;
}

void setArrayToValue(float *c, float val, size_t size)
{
    //doesn't work on floats apparently
    //memset(c, val, size);
    
    for(size_t i = 0;i<size;i++)
        *(c++) = val;
}

void setRandomValues(float* a, int size)
{
    for(int i = 0;i<size;i++)
        *(a++) = (float) (rand()) / (float) (RAND_MAX);
}

int parseArgs(int argc, char **argv, int &size, int &iter)
{
    int opt;
    while ((opt = getopt(argc, argv, "hs:i:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            cout << "help:" << endl;
            cout << "-h       prints help (this)" << endl;
            cout << "-s size  sets size (default is 128 - will be made divisible by 16)" << endl;
            cout << "-i iter  sets iterations (default is 1)" << endl;
            break;
        case 's':
        {
            int sval = atoi(optarg);
            if (sval < 1)
            {
                cout << "Invalid argument for -s: " << sval << endl;
                return 1;
            }
            size = sval;
        }
        break;
        case 'i':
        {
            int ival = atoi(optarg);
            if (ival < 1)
            {
                cout << "Invalid argument for -i: " << ival << endl;
                return 1;
            }
            iter = ival;
        }
        break;
        case '?':
            if (optopt == 'i' || optopt == 's')
                cout << "Option -" << optopt << " requires an argument.\n";
            else
                cout << "Unknown option character " << optopt << endl;
            return 1;
        }
    }
    return 0;
}

float SumOfSums(float *a, int size)
{
    float sum = 0;
    if (size % 16 == 0)
    {
        size = size / 16;
        for (int i = 0; i < size; i++)
        {
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
            sum += *(a++);
        }
    }
    else
        for (int i = 0; i < size; i++)
            sum += *(a++);
    return sum;
}

void SingleCore(float *a, float *b, float *c, int size)
{
    //cout << __FUNCTION__ << " " << hex << size << dec << " " << size << endl;
    assert((size & 0x7) == 0);
    size = size / 16;

    for (int i = 0; i < size; i++)
    {
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
        *(c++) = *(a++) + *(b++);
    }
}

void Intrinsic(float * a, float * b, float * c, int size){
    //single core, using vaddq_f32
    assert((size & 0x7) == 0);
    size = size/16;

    for(int i = 0;i<size;i++){
        float32x4_t av = vld1q_f32(a);
        float32x4_t bv = vld1q_f32(b);
        float32x4_t cv = vaddq_f32(av, bv);
        vst1q_f32(c, cv);
        a += 4;
        b += 4;
        c += 4;
        av = vld1q_f32(a);
        bv = vld1q_f32(b);
        cv = vaddq_f32(av, bv);
        vst1q_f32(c, cv);
        a += 4;
        b += 4;
        c += 4;
        av = vld1q_f32(a);
        bv = vld1q_f32(b);
        cv = vaddq_f32(av, bv);
        vst1q_f32(c, cv);
        a += 4;
        b += 4;
        c += 4;
        av = vld1q_f32(a);
        bv = vld1q_f32(b);
        cv = vaddq_f32(av, bv);
        vst1q_f32(c, cv);
        a += 4;
        b += 4;
        c += 4;
    }
}

void MultiCore(float *a, float *b, float *c, int size, int numCores)
{
    assert((size & 0x7) == 0); //multiple of 16

    if( (size % (numCores * 16) == 0) ){
        //each core gets a multiple of 16 tasks to perform
        vector<thread> threads;
        for(int i = 0;i<numCores;i++){
            threads.push_back(thread(SingleCore,a,b,c,size/numCores));
            a += size/numCores;
            b += size/numCores;
            c += size/numCores;
        }
        join_all(threads);
    }
    else{
        vector<thread> threads;
        
        int numCoresLarger = 0;
        int numCoresSmaller = numCores;

        int remainder = size%(numCores*16);

        int standardSize = (size-remainder)/numCores;

        while(remainder!=0){
            numCoresLarger++;
            numCoresSmaller--;
            remainder -= 16;
        }

        for(int i = 0;i<numCoresLarger;i++){
            threads.push_back( thread(SingleCore,a,b,c,(standardSize + 16)) );
            a += standardSize+16;
            b += standardSize+16;
            c += standardSize+16;
        }
        for(int i = 0;i<numCoresSmaller;i++){
            threads.push_back( thread(SingleCore,a,b,c,standardSize) );
            a += standardSize;
            b += standardSize;
            c += standardSize;
        }
        join_all(threads);
    }
}

void MultiIntrinsic(float * a, float * b, float * c, int size, int numCores)
{
    assert((size & 0x7) == 0); //multiple of 16

    if( (size % (numCores * 16) == 0) ){
        //each core gets a multiple of 16 tasks to perform
        vector<thread> threads;
        for(int i = 0;i<numCores;i++){
            threads.push_back(thread(SingleCore,a,b,c,size/numCores));
            a += size/numCores;
            b += size/numCores;
            c += size/numCores;
        }
        join_all(threads);
    }
    else{
        vector<thread> threads;
        
        int numCoresLarger = 0;
        int numCoresSmaller = numCores;

        int remainder = size%(numCores*16);

        int standardSize = (size-remainder)/numCores;

        while(remainder!=0){
            numCoresLarger++;
            numCoresSmaller--;
            remainder -= 16;
        }

        for(int i = 0;i<numCoresLarger;i++){
            threads.push_back( thread(Intrinsic,a,b,c,(standardSize + 16)) );
            a += standardSize+16;
            b += standardSize+16;
            c += standardSize+16;
        }
        for(int i = 0;i<numCoresSmaller;i++){
            threads.push_back( thread(Intrinsic,a,b,c,standardSize) );
            a += standardSize;
            b += standardSize;
            c += standardSize;
        }
        join_all(threads);
    }
}

void do_join(thread& t)
{
    t.join();
}
void join_all(vector<thread>& v)
{
    for(int i = 0;i<v.size();i++){
        v.at(i).join();
    }
}
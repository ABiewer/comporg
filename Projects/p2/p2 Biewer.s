.text
.global main
.align 2

main:
    str x30, [sp,-16]!
    mov x20, x1 //argv[] pointer

    //the second argument of the command line will be the name of the file
    ldr x0, [x20, 8]
    //if no argument is given, exit
    cbz x0, _end

    //store the pointer to the stat data location to x1 (for stat) and to x19 (for main)
    ldr x1, =buffer

    bl  stat
    cbnz x0, _end //if x0 is not 0, the stat call did not work

    //keep .buffer in a register
    ldr x21, =buffer

    //name
    ldr x0, =name
    ldr x1, [x20, 8]
    bl  printf

    //inode
    ldr x0, =inode
    ldr x1, [x21, 8]
    bl  printf

    //mode
    ldr x0, =mode //string for mode
    ldr x22, [x21, 16]  ///load 16 bytes from start of x21 (st_mode)
    and x1, x22, 0x1ff  //bitwise last 9 bits
    and x23, x22, 0x8000 //reg file in hex
    cbnz x23, regfile   //branch for regular file
    and x23, x22, 0x4000 //directory in hex 
    cbnz x23, direct    //branch for directory
    ldr x2, =other      //if neither a directory or regular file, it is some other file type
    bl printmode
regfile:
    ldr x2, =regular
    bl  printmode
direct:
    ldr x2, =directory
printmode:
    bl  printf

    //bytes
    ldr x0, =bytes
    ldr x1, [x21, 48]
    bl  printf

    //blocks
    ldr x0, =blocks
    ldr x1, [x21, 64]
    bl  printf

_end:
    ldr x30, [sp], 16

    ret

        .data
buffer: .space 128
name:   .string "Name:   %s\n"
inode:  .string "Inode:  %lu\n"
mode:   .string "Mode:   %o, %s\n"
bytes:  .string "Bytes:  %ld\n"
blocks: .string "Blocks: %ld\n"
regular:    .string "Regular File"
directory:  .string "Directory"
other:      .string "Other type of file"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

int main(int argc, char* argv[]){
    struct stat p1;
    
    int n = stat(argv[0], &p1);

    if(n==0){
        printf("%s\n",argv[0]);
        printf("%x\n",p1.st_mode);
        printf("%lu\n",p1.st_ino);
        printf("%ld\n",p1.st_size);
        printf("%ld\n",p1.st_blocks);
    }
    
    return 0;
}
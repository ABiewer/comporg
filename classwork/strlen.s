.file "strlen.s"
.text
.align 2
.global strlen

strlen:
    //x0 is a char *
    str x10, [x0]
    //w10 is a char *
    cbz (x10), endLoop
    //if w10 references a 0, return 0 (endloop)
Loop:
    add x10, x10, 1
    //add one to the address of w10
    cbnz (x10), Loop
    //if x10 references a 0, break from the loop
endLoop:
    //the address difference gives the length
    sub x0, x10, x0
    ret
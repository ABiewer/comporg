
.file "myputs.s"
.text
.align 2
.global myputs

myputs:
    str x30, [sp, -16]!
    //x0 already has  a char* from its function call in main
    bl  puts
    //don't need x0 as a return
    ldr x30,[sp],16
    ret

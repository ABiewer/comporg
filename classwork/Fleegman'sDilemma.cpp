//Array of 5 mutexes, one more cutex cout lock

/*
Array of threads: c++ 
while(true){
    sleep(random time between 50 and 100 ms)
    thread i gets mutex i and mutex i+1
    get cout lock
    cout thread i is fleegmaning
    release coutlock
    release mutex i and i+1
}
*/

#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>
#include <ctime>
#include <vector>

using namespace std;

mutex m[5];
mutex coutlock;

void sleeprandom(int i){
    while(true){
        if(i==0){
            m[i].lock();
            m[(i+1)%thread::hardware_concurrency()].lock();
        }
        else{
            m[(i+1)%thread::hardware_concurrency()].lock();
            m[i].lock();
        }
        this_thread::sleep_for(chrono::milliseconds(rand()%50+50));
        coutlock.lock();
        cout<<"Thread " << i << " is fleegmaning"<<endl;
        coutlock.unlock();
        m[i].unlock();
        m[(i+1)%thread::hardware_concurrency()].unlock();
    }
}

int main(){
    srand(time(0));
    std::cout<<thread::hardware_concurrency()<<endl;
    vector<thread> t(thread::hardware_concurrency());
    for(int i=0;i<thread::hardware_concurrency();i++){
        t.at(i) = thread(sleeprandom,i);
    }
    for(int i=0;i<5;i++){
        t.at(i).join();
    }
}
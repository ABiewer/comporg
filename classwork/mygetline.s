            .text
            .align 		2
            .global		main
            .equ		BUFLEN, 4096

/*	Register usage:
	x19	return code
	x20	buffered file descriptor

*/
	
main:		stp		x30, x19, [sp, -16]!
            str		x20, [sp, -16]!
            mov		x19, xzr
            mov		x20, xzr

            // ensure we have enough arguments
            ldr		x0, [x1, 8]
            cbnz		x0, have_arg
            // not enough args
            ldr		x0, =not_enough_args
            bl		puts
            mov		x19, 1
            b		my_exit

have_arg:	//bl		puts
            //b		my_exit
            mov		w1, wzr
            bl		open
            cmp		w0, wzr
            bge		file_is_open

            // file did not open
            ldr		x0, =no_open
            bl		perror
            mov		x19, 1
            b		my_exit

file_is_open:
            mov		w20, w0
            ldr		x0, =good_open
            //bl		puts
            //b		my_exit

            mov w0, w20
            ldr x1, =buffer
            mov w2, 1
		    bl		mygetline

            cmp w0, wzr
            bne goodGetline
            mov x19, 1
            b   my_exit

goodGetline:
            ldr x0, =buffer
            bl  puts

my_exit:	cmp		w20, wzr
		    blt		closed

		    mov		w0, w20
		    bl		close

closed:		mov		x0, x19
		    ldr		x20, [sp], 16
		    ldp		x30, x19, [sp], 16
		    ret	

/*	Register usage
	w19	fd	
	w20	buffer (char*)
    w21 buffer_length
    w22 retval
    w23 bytes_read
    w24 counter(i)
*/

/**
 * 
 */

mygetline:	stp		x30, x19, [sp, -16]!
		    stp		x20, x21, [sp, -16]!
            stp     x22, x23, [sp, -16]!
            str     x24, [sp, -16]!

            //line 92
            mov w19, w0
            mov w20, w1
            mov w21, w2

            //lines 94,95
            mov w22, wzr
            mov w23, wzr

            //line 97
            mov w0, w20
            mov x1, xzr
            mov w2, w21
            bl  memset

            //for initialize
            mov w24, wzr
mygetlineLoop: 
            //for comparison
            sub w10, w21, 1
            cmp w24, w10
            bge mygetlineReturn

            //line 99
            mov w0, w19
            add w10, w20, w24
            mov w1, w10
            mov x2, 1
            bl  read
            mov w23, w0

            //line 100
            cbz w23, mygetlineReturn

            //line 102
            mov w22, 1

            //line 103
            add w10, w20, w24
            ldr w10, [x10]
            ldr w11, =newlinechar
            ldr w11, [x11]
            cmp w10, w11
            beq mygetlineReturn

            //for increment
            add w24, w24, 1
            b   mygetlineLoop

mygetlineReturn:
            mov w0, w22
            ldr     x24, [sp], 16
            ldp     x22, x23, [sp], 16
		    ldp		x20, x21, [sp], 16
		    ldp		x30, x19, [sp], 16

		    ret

		    .data

fbuf:			    .space		BUFLEN
buffer:             .space      1
not_enough_args:	.asciz		"Not enough args"
no_open:		    .asciz		"open failed"
good_open:		    .asciz		"good open"
newlinechar:        .asciz      "\n"
		    .end
